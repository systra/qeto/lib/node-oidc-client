module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true
    },
    parser: '@typescript-eslint/parser',
    plugins: [
        '@typescript-eslint'
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    parserOptions: {
        ecmaVersion: 13,
        sourceType: 'module',
    },
    rules: {
        'indent': [1, 4, { 'SwitchCase': 1 }],
        'linebreak-style': [1, 'unix'],
        'semi': ['warn', 'always'],
        'quotes': [1, 'single'],
        'no-multiple-empty-lines': [1, { 'max': 2, 'maxEOF': 0, 'maxBOF': 0 }],
        'comma-dangle': [1, 'only-multiline'],
        'generator-star-spacing': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'space-before-function-paren': 'off',
        'no-new': 'off',
        'no-extra-boolean-cast': 'off',
        'no-template-curly-in-string': 'off',
        'no-process-env': 'off',
        'no-prototype-builtins': 'off',
        'no-trailing-spaces': 'warn',
        'dot-notation': 'off',
        'no-async-promise-executor': 'off',
        'prefer-promise-reject-errors': 'off',
        'no-empty': 'off',
        'no-extra-semi': 'warn',
        'no-mixed-spaces-and-tabs': 'warn',
        'no-undef': 'warn',
        'no-unused-vars': 'off',
        'no-useless-escape': 'warn',
        '@typescript-eslint/no-unused-vars': 'warn'
    },
    globals: {
        NodeJS: true,
        beforeEach: 'readonly',
        Buffer: 'readonly',
        describe: 'readonly',
        Exception: 'readonly',
        expect: 'readonly',
        it: 'readonly',
        jest: 'readonly',
        process: 'readonly',
        Autodesk: 'readonly',
        JSX: 'readonly',
    }
};
