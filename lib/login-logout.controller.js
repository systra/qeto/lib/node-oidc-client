
const OidcClientService = require('./service').OidcClientService
const { generators } = require('openid-client')
class LoginLogoutController {

    async authorize(req, res, next) {
        try {
            let code_challenge;
            let code_challenge_method
            if (!req.query.code_challenge || !req.query.code_challenge_method) {
                const codeVerifier = generators.codeVerifier();

                OidcClientService.set_in_session(req, 'code_verifier', codeVerifier)
                OidcClientService.set_in_session(req, 'has_code_verifier_in_session', true)

                code_challenge = generators.codeChallenge(codeVerifier)
                code_challenge_method = 'S256'
            } else {
                code_challenge = req.query.code_challenge;
                code_challenge_method = req.query.code_challenge_method;
            }

            if (req.query.next) {
                const nextUrl = new URL(req.query.next);
                if (req.query.noquery) {
                    nextUrl.searchParams.append('noquery', 'true')
                }
                if (OidcClientService.isDebug()) console.log('authorize: Should be checking domain of next, but doing nothing')
                if (!OidcClientService.isDebug()) {
                    // TODO: check the next url here to make sure it comes from the same domain
                    // local dev output: Authorize request: host=localhost:5000, next=http://localhost:3000/oidclogin
                }
                OidcClientService.set_in_session(req, 'oidc_next_url', nextUrl.href)
            }

            const client = await OidcClientService.client()
            const authorizationUrl = client.authorizationUrl({
                scope: OidcClientService._config.scope,
                code_challenge,
                code_challenge_method
            })

            req.session.save(() => {
                res.redirect(authorizationUrl)
            });
        } catch (err) {
            next(err)
        }
    }

    async authorizationCallback(req, res, next) {
        const client = await OidcClientService.client();
        let params = client.callbackParams(req);
        const oidc_next_url = OidcClientService.get_from_session(req, 'oidc_next_url');

        const has_code_verifier_in_session = OidcClientService.get_from_session(req, 'has_code_verifier_in_session');
        if (has_code_verifier_in_session) {
            const code_verifier = OidcClientService.get_from_session(req, 'code_verifier');
            const { refresh_token, id_token, access_token } = await OidcClientService.getToken({
                grant_type: 'authorization_code',
                code: params.code,
                code_verifier
            });
            OidcClientService.set_in_session(req, 'oidc_refresh_token', refresh_token);
            OidcClientService.set_in_session(req, 'id_token', id_token);
            OidcClientService.set_in_session(req, 'access_token', access_token);
            OidcClientService.set_in_session(req, 'has_code_verifier_in_session', null)
            OidcClientService.set_in_session(req, 'code_verifier', null)
            OidcClientService.set_in_session(req, 'oidc_next_url', null)
            params = { access_token, id_token }
        }
        if (!OidcClientService.isDebug()) {
            // TODO: check the next url here to make sure it comes from the same domain
            // local dev output: Authorize request: host=localhost:5000, next=http://localhost:3000/oidclogin
        }
        let keys = ['access_token']
        const next_url = new URL(oidc_next_url);
        if (next_url.searchParams.has('keys')) {
            keys = next_url.searchParams.get('keys').split(' ')
            next_url.searchParams.delete('keys')
        }
        const query = Object.keys(params)
            .filter(k => keys.includes(k))
            .map(key => `${key}=${params[key]}`)
            .join('&');

        if (oidc_next_url) {
            const next_url = new URL(oidc_next_url);
            const url = encodeURI(`${next_url}?${query}`);

            if (OidcClientService.isDebug()) {
                console.log('authorization callback: uri for redirect')
                console.log(url);
            }

            req.session.save(() => {
                res.header('Access-Control-Allow-Credentials', 'true');
                res.redirect(url);
            });
        } else {
            res.send(query);
        }
    }

    async token(req, res, next) {
        try {
            if (OidcClientService.isDebug()) {
                console.log('token: Requested body')
                console.log(JSON.stringify(req.body));
            }
            if (!OidcClientService.isDebug()) {
                // TODO: check the next url here to make sure it comes from the same domain
                // local dev output: Authorize request: host=localhost:5000, next=http://localhost:3000/oidclogin
            }
            const token_set = await OidcClientService.getToken(req.body);
            OidcClientService._config.on_auth_succeeded(req, token_set)
            res.status(200).send(token_set);
        } catch (err) {
            next(err)
        }

    }

    async logout(req, res) {
        let state;
        const client = await OidcClientService.client()
        let token_set = {
            id_token: req.query.id_token,
            access_token: req.query.access_token,
        };

        try {
            OidcClientService.revoke(token_set)
        } catch (err) { if (OidcClientService.isDebug()) console.log(err) }

        if (req.query.next) {
            if (OidcClientService.isDebug()) console.log(`Logout request: host=${req.headers.host}, next=${req.query.next}`)
            if (!OidcClientService.isDebug()) {
                if (OidcClientService.isDebug()) console.log('logout: Should be checking domain of next, but doing nothing')
                // TODO: validate domain
            }
            state = req.query.next
        }

        const endSessionUrl = client.endSessionUrl({
            state,
            // Without id token hint, we can log back on again without re-entering password
            id_token_hint: token_set
        })
        OidcClientService.destroy_session(req)
        res.redirect(endSessionUrl)
    }

    async logoutCallback(req, res, next) {
        try {
            OidcClientService.destroy_session(req)
            if (req.query.state) {
                res.redirect(req.query.state)
            } else {
                res.send('You logged out')
            }
        } catch (err) {
            next(err)
        }
    }
}

module.exports = {
    LoginLogoutController
}