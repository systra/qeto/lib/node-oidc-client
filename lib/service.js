const { Issuer, } = require('openid-client');
class OidcClientServiceClass {

    init(config) {
        this._config = config || {};
        if (this.isDebug()) console.log('Initializing OidcClientService');
        this._memcached = new NodeCache();
    }

    isDebug() { return this._config.is_debug };

    get_from_session(req, key) {
        return this._config.get_from_session(req, key)
    }
    set_in_session(req, key, value) { return this._config.set_in_session(req, key, value) };
    is_token_blacklist(id_token) {
        return this._config.is_token_blacklist(id_token)
    };
    blacklist_token(id_token, exp) { return this._config.blacklist_token(id_token, exp) };
    destroy_session(req) { return this._config.destroy_session(req) };

    init(config) {
        this._config = config;
        if (this.isDebug()) console.log('Initializing OidcClientService');
    }

    isDebug() { return this._config.is_debug };

    async client() {
        if (!this._client) {
            let issuer = await this.issuer();
            const metadata = {};
            for (var prop of ['client_id', 'client_secret', 'redirect_uris', 'post_logout_redirect_uris']) {
                if (this._config[prop] === undefined) {
                    throw new Error(`Missing required config parameter for oidc Client ${prop}`);
                }
                metadata[prop] = this._config[prop];
            }

            metadata['response_types'] = ['code'];

            this._client = new issuer.Client(metadata);
        }
        return this._client;
    }

    async issuer() {
        if (!this._issuer) {
            if (this._config.op_metadata_url === undefined) {
                throw new Error('Missing required parameter for oidc issuer op_metadata_url');
            }
            this._issuer = await Issuer.discover(this._config.op_metadata_url);
        }
        return this._issuer;
    }

    getTokenFromHeader(req) {
        if (!req.headers.authorization) {
            throw new Error('Missing authorization header');
        }
        return req.headers.authorization.replace('Bearer ', '');
    }

    async getToken(body) {
        const { code_verifier, ...params } = body
        const client = await this.client();
        const token_set = await client.callback(this._config.redirect_uris[0], params, { code_verifier });
      
        return token_set
    }

    async verifyIdToken(id_token) {
        // token in deny list?
        const inDenyList = OidcClientService.is_token_blacklist(id_token);
        if (inDenyList) {
            throw new Error('Revoked token')
        }

        const client = await OidcClientService.client();
        const { payload } = await client.validateJWT(id_token, OidcClientService._config.id_token_encrypted_response_alg ?
            OidcClientService._config.id_token_encrypted_response_alg : 'RS256');

        return payload;
    }

    async verifyAccessToken(access_token) {
        // token in deny list?
        const inDenyList = OidcClientService.is_token_blacklist(access_token);
        if (inDenyList) {
            throw new Error('Revoked token')
        }

        const client = await OidcClientService.client();
        const userinfo = await client.userinfo(access_token); //Try to get user info
        const decodedToken = JSON.parse(Buffer.from(access_token.split('.')[1], 'base64'))
        return {
            ...decodedToken,
            ...userinfo,
            email:
                userinfo.email ? userinfo.email
                : decodedToken.email ? decodedToken.email
                : decodedToken.unique_name && decodedToken.unique_name.endsWith('systra.info') ? decodedToken.unique_name.replace('systra.info', 'systra.com')
                : undefined
        };
    }

    async revoke(token_set) {
        const client = await OidcClientService.client();
        if (token_set.access_token) {
            const payload = await client.userinfo(token_set.access_token);
            const exp = payload.exp - payload.iat;
            OidcClientService.blacklist_token(token_set.access_token, exp);
        }
        if (token_set.id_token) {
            const { payload } = await client.validateJWT(id_token, OidcClientService._config.id_token_encrypted_response_alg ?
                OidcClientService._config.id_token_encrypted_response_alg : 'RS256');
            const exp = payload.exp - payload.iat;
            OidcClientService.blacklist_token(token_set.id_token, exp);
        }
    }
}

var OidcClientService = new OidcClientServiceClass()

module.exports = {
    OidcClientService
}