module.exports = {
    OidcClientService: require('./service').OidcClientService,
    ApiController: require('./api.controller').ApiController,
    LoginLogoutController: require('./login-logout.controller').LoginLogoutController,
    getUserFromAccessToken: require('./middleware').getUserFromAccessToken,
    getUserFromIdToken: require('./middleware').getUserFromIdToken,
    OidcClientRouter: require('./routes'),
    generators : require('openid-client').generators
}
