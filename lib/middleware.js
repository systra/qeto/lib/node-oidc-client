const OidcClientService = require('./service').OidcClientService


function getUserFromIdToken(req, res, next) {
    getUserFromToken(req, res, next, OidcClientService.verifyIdToken)
}

function getUserFromAccessToken(req, res, next) {
    getUserFromToken(req, res, next, OidcClientService.verifyAccessToken)
}

async function getUserFromToken(req, res, next, verifyTokenMethod) {
    try {
        const token = OidcClientService.getTokenFromHeader(req);
        const payload = await verifyTokenMethod(token);
        if (Array.isArray(OidcClientService._config.select_for_user)) {
            const user = {};
            for (const key of OidcClientService._config.select_for_user) {
                if (payload[key] === undefined) {
                    throw new Error(`Missing required parameter in decoded token ${key}`);
                }
                user[key] = payload[key]
            }
            req.user = user;
        }
        else {
            req.user = payload
        }
        next();
    } catch (err) {
        if (OidcClientService.isDebug()) console.log(err);
        res.status(401);
        res.header("WWW-Authenticate", 'Bearer authorization_uri=' + OidcClientService._config.op_metadata_url)
        res.send('Unauthorized')
    }
}


module.exports = {
    getUserFromAccessToken,
    getUserFromIdToken
}