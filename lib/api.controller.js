const OidcClientService = require('./service').OidcClientService

class ApiController {
    async refresh(req, res, next) {
        try {
            const client = await OidcClientService.client();
            let refreshToken; //Get refresh token from body, otherwise check in session
            if (req.body && req.body.grant_type == 'refresh_token' && req.body.refresh_token) {
                refreshToken = req.body.refresh_token;
            } else {
                refreshToken = OidcClientService.get_from_session(req, 'oidc_refresh_token');
                if (!refreshToken) {
                    throw new Error('Cannot refresh token not found in session');
                }
            }
            const token_set = await client.refresh(refreshToken);
            const { refresh_token, access_token, id_token } = token_set;
            OidcClientService.set_in_session(req, 'oidc_refresh_token', refresh_token);
            res.json({ access_token, id_token });
        } catch (err) {
            if (OidcClientService.isDebug()) console.log(err);
            res.status(400).send();
        }
    }
}

module.exports = {
    ApiController
}