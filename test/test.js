const {  OidcClientService, OidcClientRouter, getUserFromIdToken, getUserFromAccessToken } = require( '../lib');
const { generators, TokenSet } = require('openid-client')
const express = require('express')
const session  = require('express-session')
const SqliteStore = require('better-sqlite3-session-store')(session)
const sqlite = require('better-sqlite3')
const  dotenv = require('dotenv');
dotenv.config({ path: './.env' });

const app = express();
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

const db = new sqlite("sessions.db");

const sessionParams = {
    resave: false,
    secret: 'thisIsTheBestSecretEver',
    store: new SqliteStore({
        client: db, 
        expired: {
          clear: true,
          intervalMs: 900000 //ms = 15min
        }
    }),
    saveUninitialized: false, // TODO
    cookie: {
        // sameSite: 'none',
        // secure: false,            //setting this false for http connections
        // maxAge: 3600000,
    }
};
app.use(session(sessionParams));

const blacklistedToken = [];

OidcClientService.init({
    client_id: process.env.OP_CLIENT_ID,
    client_secret: process.env.OP_CLIENT_SECRET,
    redirect_uris: [process.env.REDIRECT_URI],
    post_logout_redirect_uris: [process.env.POST_LOGOUT_REDIRECT_URI],
    op_metadata_url: `${process.env.OP_URL}${process.env.OP_METADATA_ROUTE}`,
    scope: 'openid profile email',
    select_for_user: ['email', 'oid'],
    is_debug: true,
    get_from_session: (req, key) => {
        return req.session[key]
    },
    set_in_session: (req, key, value)=>{
        req.session[key] = value;
    },
    blacklist_token: (id_token)=>{
        blacklistedToken.push(id_token)
    },
    is_token_blacklist: (id_token)=>{
        return blacklistedToken.includes(id_token)
    },
    destroy_session : (req)=>{
        req.session.destroy((err)=>{
            console.log(err);
        });
    },
    on_auth_succeeded: (token_set)=>{
        OidcClientService.verifyIdToken(token_set.id_token).then((user)=>{
            console.log(user);
        })
    }
})


app.use('/auth', OidcClientRouter)
app.use('/api/*', getUserFromAccessToken);
app.use('/api/test', (req, res, next) =>{
    res.status(200).send(req.user)
})
app.listen('5000', ()=>{
    console.log('listenning');
});