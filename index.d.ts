import { Request, Router, Response, NextFunction } from 'express';
import * as OidcClient from 'openid-client';
declare module 'express-session' {
    interface SessionData {
        [key: string]: any;
    }
}
declare module '@systra/oidc-client' {
    export interface APIRequest<ReqParams = unknown, ResBody= unknown, ReqBody= unknown, ReqQuery= unknown> 
    extends Request<ReqParams, ResBody, ReqBody, ReqQuery> {
        user: {
            [key: string]: string;
        }
    }

    export interface OidcClientConfig {
        client_id: string;
        client_secret: string;
        redirect_uris: Array<string>;
        post_logout_redirect_uris: Array<string>;
        op_metadata_url: string;
        scope: string; //'Openid email' are required
        select_for_user: Array<string>;
        is_debug: boolean;
        get_from_session: <T>(req: APIRequest, key: string) => T;
        set_in_session : <T>(req: APIRequest, key:string, value:T) => void;
        is_token_blacklist: (id_token: string) => boolean;
        blacklist_token: (id_token: string, exp: number) => void;
        destroy_session: (req: APIRequest) => void;
        on_auth_succeeded: (req: APIRequest , token_set: OidcClient.TokenSet)=> void
    }

    export class OidcClientServiceClass {
        init(config: OidcClientConfig): void;
        isDebug(): boolean;
        verifyIdToken:(token: string)=> Record<string, string>
    }

    export let OidcClientService: OidcClientServiceClass;
    export let OidcClientRouter: Router;
    export function getUserFromAccessToken(req: Request, res: Response, next: NextFunction): Promise<void>;
    export function getUserFromIdToken(req: Request, res: Response, next: NextFunction): Promise<void>;
}